FROM php:8.1.0-fpm

# Set working directory
WORKDIR /var/www/starfund.app

# Install dependencies
RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libjpeg-dev \
    libpng-dev \
    libfreetype6-dev \
    libbz2-dev \
    unzip \
    zip \
    nano \
    cron \
    libbz2-dev \
    libzip-dev \
    mariadb-client \
    default-mysql-client \
    exiftool \
    supervisor \
    && docker-php-ext-configure gd --with-jpeg --with-freetype \
    && docker-php-ext-install -j$(nproc) gd bcmath \
    && docker-php-ext-install pdo pdo_mysql bz2 exif zip \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure exif \
    && docker-php-ext-enable exif \
    && docker-php-ext-configure bz2 --with-bz2 \
    && docker-php-ext-install opcache sockets pcntl

COPY ./opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY --from=composer:2.0 /usr/bin/composer /usr/local/bin/composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

COPY . /var/www/starfund.app

# Install dependencies
RUN composer install
ADD ./.docker/supervisor/worker.conf /etc/supervisor/conf.d/worker.conf

#RUN chown -R www-data:www-data /var/www/starfund.app/storage
#RUN chown -R www-data:www-data /var/www/starfund.app/public

#RUN chmod -R 755 /var/www/starfund.app/storage

EXPOSE 9000

